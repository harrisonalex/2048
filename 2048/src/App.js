import React, {Component} from 'react';
import './App.css';


class Cell extends Component {

  render() {
    return (
      <div className="Board__cell">
        {this.props.value && <span className="Board__value">{this.props.value}</span>}
      </div>
    )
  }
}

class Board extends Component {
  render()
  {
    let rows = this.props.board.map((row, i) => {
      return (
        <div className="Board__row" key={i}>
          {row.map((col, j) => {
            return (
              <Cell key={j} value={this.props.board[i][j]}/>
            )
          })}
        </div>
      )
    })
    return (
      <div className="Board">
        {rows}
      </div>
    )
  }
}


class App extends Component {
  constructor(props)
  {
    let board = [[null, null, null, null],
            [null, null, null, null],
            [null, null, null, null],
            [null, null, null, null]];
    let randomCell = selectRandomCell();
    board[randomCell.y][randomCell.x] = 2;

    super(props);
    this.state = {
      board: board,
      isEndGame: false,
    }
  }

  handleUp()
  {
    alert("Вверх");
  }

  handleDown()
  {
    alert("Вниз");
  }

  handleLeft()
  {
    alert("Влево");
  }

  handleRight()
  {
    alert("Вправо");
  }
  handleKeyPress(e)
  {
    // Keycodes:
    // Вверх: 38
    // Влево: 37
    // Вправо: 39
    // Вниз: 40
    const UP = 38;
    const LEFT = 37;
    const RIGHT = 39;
    const DOWN = 40;
    if (!this.state.isEndGame)
      switch(e.keyCode)
      {
        case UP:
          this.handleUp();
          return;
        case LEFT:
          this.handleLeft();
          break;
        case RIGHT:
          this.handleRight();
          break;
        case DOWN:
          this.handleDown();
          break;
        default:
          return;
      }
    else {
      alert("Вы проиграли, увы");
    }
  }


  componentDidMount()
  {
    // Отслеживаем нажатие кнопок "Вверх", "Вниз", "Влево" и "Вправо"
    document.addEventListener('keydown', this.handleKeyPress.bind(this));
  }


  render()
  {
    return (
      <div className="App">
        <Board board={this.state.board} />
      </div>
    )
  }
}

// Вспомогательные функции
function selectRandomCell()
{
  const x = Math.floor(Math.random()*4);
  const y = Math.floor(Math.random()*4);
  return {
    x: x,
    y: y
  }
}

export default App;
